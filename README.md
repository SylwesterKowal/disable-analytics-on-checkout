# Wyłącz Google Analytics w Procesie Koszyka

<b>Instalacja:</b>

`composer require m21/disableanalyticscheckout`


Wyłącznie Google Analytics na niektórych stronach koszyka. 
Kod analytics jest blokowany przez AdBlokery instalowane w przeglądarkach co generuje błędy, które uniemożliwiają zakończenie procesu sprzedaży


# Moduły Magento

`https://magento.21w.pl`


# Informacje

21w.pl | 
Sylwester Kowal  | 
tel +48 608012047